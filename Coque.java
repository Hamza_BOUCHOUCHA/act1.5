package Bateau;

public class Coque {
	
	public int id;
	public String matiere;
	public int size;

	
	public Coque(int id) {
		super();
		this.id = id;
	}


	public Coque(int id, String matiere, int size) {
		super();
		this.id = id;
		this.matiere = matiere;
		this.size = size;
	}
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getMatiere() {
		return matiere;
	}


	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}


	public int getSize() {
		return size;
	}


	public void setSize(int size) {
		this.size = size;
	}
	public String toString() {
		return " Coque N°" + id ;
	}
     
	
	
}
