package Bateau1;

public class Pont {
	
	public int id;
	public int size;
	public int capacite;
	
	
	public Pont(int id) {
		super();
		this.id = id;
	}

	public Pont(int id, int size, int capacite) {
		super();
		this.id = id;
		this.size = size;
		this.capacite = capacite;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getCapacite() {
		return capacite;
	}

	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}
	
	public String toString() {
		return " Pont N°" + id ;
	}
	
	

}
