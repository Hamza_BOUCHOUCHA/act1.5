package Bateau;

public class Mat {
	private int id;
	private String matiere;
	
    
	public Mat(int id) {
		super();
		this.id = id;
	}
	
	public Mat(int id, String matiere) {
		super();
		this.id = id;
		this.matiere = matiere;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMatiere() {
		return matiere;
	}

	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}
 
	public String toString() {
		return " Mat N°" + id ;
	}
	

	
    
}
