package Bateau;

public class Cabine {
	
	public int id;
	public int num;
	public int num_personne;
	
	
	public Cabine(int id) {
		super();
		this.id = id;
	}
	
	public Cabine(int id, int num, int num_personne) {
		super();
		this.id = id;
		this.num = num;
		this.num_personne = num_personne;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getNum_personne() {
		return num_personne;
	}

	public void setNum_personne(int num_personne) {
		this.num_personne = num_personne;
	}
	
	@Override
	public String toString() {
		return "Cabine n°" + id ;
	}

}
