package Bateau;

public class Bateau {

	public int id;
	public String name;
	public int old;
	
	public Bateau(int id) {
		super();
		this.id = id;
	}
	
	public Bateau(int id, String name, int old) {
		super();
		this.id = id;
		this.name = name;
		this.old = old;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOld() {
		return old;
	}

	public void setOld(int old) {
		this.old = old;
	}

	@Override
	public String toString() {
		return "Le bateau n°" + id ;
	}
	
	
	
}
