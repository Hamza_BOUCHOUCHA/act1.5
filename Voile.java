package Bateau;

public class Voile {
	
	public int id;
	private String tissu;
	public int size;
	protected int weight;
	public static boolean enrouler;
	public boolean derouler;
	
	
	public Voile(int id, String tissu, int size, int weight) {
		super();
		this.id = id;
		this.tissu = tissu;
		this.size = size;
		this.weight = weight;
	}
	
	
	public Voile(int id) {
		super();
		this.id = id;
	}
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTissu() {
		return tissu;
	}


	public void setTissu(String tissu) {
		this.tissu = tissu;
	}


	public int getSize() {
		return size;
	}


	public void setSize(int size) {
		this.size = size;
	}


	public int getWeight() {
		return weight;
	}


	public void setWeight(int weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return " Voile N°" + id ;
	}
     
	

}
